from window import Window
from flask import Flask, request, render_template
import time, atexit, os, git

def safe_exit():
    global my_window
    my_window.kill_window()

def update_code():
    # Pull the latest changes
    repo = git.Repo('.')
    origin = repo.remotes.origin
    origin.pull()

def get_changelog():
    g = git.Git('.')
    log_string = g.log("--date=short", "--pretty='%ad: %s'", "ORIG_HEAD..")
    return log_string.splitlines()

# Create a window object that we can interact with
my_window = Window()
# If we do force exit the program, make sure we do not leave window
# in a bad state
atexit.register(safe_exit)

# Create our flask web server
app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def toggle():
    global my_window
    error = None
    if request.method == 'POST':
        my_window.toggle_window()

    return render_template('toggle.html',
                           error=error, 
                           state_string=my_window.get_window_state(), 
                           time_string=my_window.get_last_time())


@app.route('/settings', methods=['POST', 'GET'])
def settings():
    global my_window
    error = None
    if request.method == 'POST':
        settings_dict = {}
        for item in request.form:
            if request.form[item]:
                settings_dict[item] = request.form[item]
        my_window.set_settings(settings_dict)
            
    return render_template('settings.html',
                           error=error,
                           my_settings=my_window.get_settings())

@app.route('/update', methods=['POST', 'GET'])
def update():
    error = None
    if request.method == 'POST':
        for item in request.form:
            if item == "update":
                update_code()
            
    return render_template('update.html',
                           error=error,
                           changelog=get_changelog())

if __name__ == '__main__':
        app.debug = True
        app.run(host='0.0.0.0')
