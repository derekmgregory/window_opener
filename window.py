import time, os
import RPi.GPIO as io 
import ConfigParser

io.setmode(io.BCM) 

class Window:
    time_last_set = "Never"

    @property
    def window_state(self):
        return io.input(self.window_settings["sensor_pin"])
    
    def __init__(self):    
        self.window_settings = {}
        self.read_settings()
        
        # Set the motor GPIO as output, and to default OFF/0v
        io.setup(self.window_settings["open_pin"], io.OUT, initial=io.LOW)
        io.setup(self.window_settings["close_pin"], io.OUT, initial=io.LOW)

        # Set the sensor GPIO to input 
        io.setup(self.window_settings["sensor_pin"], io.IN)

    def read_settings(self):
        config = self.get_settings_from_file()
        # Stored as a dict
        self.window_settings["sensor_pin"] = config.getint("settings", "sensor_pin")
        self.window_settings["open_pin"] = config.getint("settings", "open_pin")
        self.window_settings["close_pin"] = config.getint("settings", "close_pin")
        self.window_settings["open_time"] = config.getfloat("settings", "open_time")
        self.window_settings["close_time"] = config.getfloat("settings", "close_time")

    def get_settings_from_file(self):
        config = ConfigParser.ConfigParser()
        settings_file = os.path.join(os.getcwd(),'settings.ini')

        if not os.path.isfile(settings_file):
            # No settings file, create it
            open(settings_file, "w").close()

        config.read(settings_file)

        if not config.has_section('settings'):
            config.add_section('settings')

        if not config.has_option('settings', 'sensor_pin'):
            config.set('settings', 'sensor_pin', '13')

        if not config.has_option('settings', 'open_pin'):
            config.set('settings', 'open_pin', '23')

        if not config.has_option('settings', 'close_pin'):
            config.set('settings', 'close_pin', '25')

        if not config.has_option('settings', 'open_time'):
            config.set('settings', 'open_time', '2.0')

        if not config.has_option('settings', 'close_time'):
            config.set('settings', 'close_time', '2.0')

        with open(settings_file, "w") as fp:
            config.write(fp)

        return config
    
    def toggle_window(self):
        if self.window_state:
            self.close_window()
        else:
            self.open_window()

    def open_window(self):
        if not self.window_state:
            self.turn_pin_on(self.window_settings["open_pin"],
                             self.window_settings["open_time"])

    def close_window(self):
        if self.window_state:
            self.turn_pin_on(self.window_settings["close_pin"],
                             self.window_settings["close_time"])
  
    def turn_pin_on(self, pin, timeout):
        self.time_last_set = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        io.output(self.window_settings["open_pin"], False)
        io.output(self.window_settings["close_pin"], False)    
        io.output(pin, True)
        time.sleep(timeout)
        io.output(pin, False)

    def kill_window(self):
        io.output(self.window_settings["open_pin"], False)
        io.output(self.window_settings["close_pin"], False)

    def get_window_state(self):
        if self.window_state:
            return "Open"
        else:
            return "Closed"

    def get_last_time(self):
        return self.time_last_set

    def set_settings(self, settings_dict):
        config = ConfigParser.ConfigParser()
        settings_file = my_file = (os.path.join(os.getcwd(),'settings.ini'))
        config.read(settings_file)
        
        for item in settings_dict:
            self.window_settings[item] = settings_dict[item]
        
        for key, value in self.window_settings.iteritems():
            config.set("settings", key, value)
        
        with open(os.path.join(os.getcwd(),'settings.ini'), 'w') as configfile:
           config.write(configfile)

        # Reset the window with new settings
        self.__init__()

    def get_settings(self, key=""):
        if key:
            return self.window_settings[key]
        else:
            return self.window_settings
